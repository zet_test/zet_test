<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/vendor/autoload.php';


if (isset($_POST['city']) && !empty($_POST['city'])) {

    $providers = [
        'Zet\app\Provider\YahooProvider',
        'Zet\app\Provider\OpenWeatherMapProvider',
    ];

    $apiKeys = [
        'YahooProvider' =>   '',
        'OpenWeatherMap' => '1589fd3b0d8d6f31f3044472d6f6215a',
    ];

    $temperature = new Zet\app\Provider\DelegatingProvider($providers, $apiKeys);
    $location = new Zet\app\Location\Location($_POST['city']);

    $weather = $temperature->fetch($location);
    $data['provider'] = $weather->getProviderName();
    $data['temperature'] = $weather->getTemperature();

    echo json_encode($data);
}



