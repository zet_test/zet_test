<?php

namespace zet\app\Parser;

use Zet\app\Exception\WeatherException;

class OpenWeatherMapParser 
{
    /**
     * Function decode json and get temperature from response
     * @param type $json
     * @return array
     */
    public function getTemperature($json)
    {
        try {
            $obj = json_decode($json, true);
            $temperature = $obj['main']['temp'];
        } catch (\Exception $e) {
            // Writing log, or to do something
        } catch (WeatherException $e) {
            // Writing log, or to do something
        }

        return $temperature;
    }
    
}
