<?php

namespace zet\app\Weather;


class Weather
{
    private $temperature;
    private $providerName;

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function getProviderName()
    {
        return $this->providerName;
    }

    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
    }

    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;
    }

}
