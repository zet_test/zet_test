<?php

namespace zet\app\Provider;

use Zet\app\Exception\WeatherException;
use Zet\app\Provider\WeatherProviderInterface;
use Zet\app\Location\Location;
use Zet\app\Weather\Weather;
use Zet\app\Parser\OpenWeatherMapParser;

class OpenWeatherMapProvider implements WeatherProviderInterface
{
    private $appId;
    private $parser;
    
    public function __construct(OpenWeatherMapParser $parser, $appId)
    {
        $this->appId = $appId;
        $this->parser = $parser;
    }

    /**
     * 
     * @param Location $location
     * @return Weather
     */
    public function fetch(Location $location)
    {
        $weather = new Weather();
        $city = $location->getCity();

        $json = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$city.',lt&units=metric&appid='.$this->appId);
        $temperature = $this->parser->getTemperature($json);

        $weather->setTemperature($temperature);
        $weather->setProviderName("OpenWeatherMap");

        return $weather;
    }
}
