<?php
namespace zet\app\Provider;

use Zet\app\Provider\WeatherProviderInterface;
use zet\app\Location\Location;
use Zet\app\Parser\OpenWeatherMapParser;
use Zet\app\Parser\YahooParser;
use Zet\app\Weather\Weather;

class DelegatingProvider implements WeatherProviderInterface
{
    private $appId;
    private $providers;

    public function __construct($providers, $appId)
    {
        $this->providers = $providers;
        $this->appId = $appId;
    }

    /**
     * Function get provider from array and fetch temperature
     * @param Location $location
     * @return Weather
     */
    public function fetch(Location $location)
    {
        foreach ($this->providers as $provider) {

            switch ($provider) {
                case 'Zet\app\Provider\YahooProvider':
                    $provider = new $provider(new YahooParser());
                    break;
                case 'Zet\app\Provider\OpenWeatherMapProvider':
                    $provider = new $provider(new OpenWeatherMapParser(), $this->appId['OpenWeatherMap']);
                    break;
            }

            $weather = $provider->fetch($location);

            if (!is_null($weather->getTemperature())) {
                return $weather;
            }

            continue;
        }
    }
}
