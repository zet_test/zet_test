<?php

namespace Zet\app\Provider;

use Zet\app\Location\Location;
use Zet\app\Weather\Weather;

interface WeatherProviderInterface
{
    public function fetch (Location $location);
}
