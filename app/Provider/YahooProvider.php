<?php

namespace Zet\app\Provider;

use Zet\app\Location\Location;
use Zet\app\Weather\Weather;
use Zet\app\Parser\YahooParser;

class YahooProvider implements WeatherProviderInterface
{
    public function __construct(YahooParser $parser)
    {
        $this->parser = $parser;
    }
    
    /*
     * Function get weather data from app yahoo parse it and return created weather object
     * @param Location $location
     * @return Weather
     * @throws WeatherException
     */
    public function fetch(Location $location)
    {
        $weather = new Weather();
        $city = $location->getCity();

        $baseUrl = "http://query.yahooapis.com/v1/public/yql";
        $yqlQuery = 'select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text="'.$city.'") and u="C"';
        $yqlQueryUrl = $baseUrl . "?q=" . urlencode($yqlQuery) . "&format=json";
        // Make call with cURL
        $session = curl_init($yqlQueryUrl);
        curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
        $json = curl_exec($session);

        $temperature = $this->parser->getTemperature($json);

        $weather->setTemperature($temperature);
        $weather->setProviderName("Yahoo");

        return $weather;     
    }
}
